#!/bin/bash
# OpenDelta for Universal Developers
cd install/xdelta3-3.0.7
./configure
make
cp xdelta3 ../../software
cd ..
gcc -o zipadjust zipadjust.c zipadjust_run.c -lz
cp zipadjust ../software
cd ..
rm -rf install.sh

Autonomus Delta Installation
=========================

Software to create needed files for OpenDelta modified to get installed and used easily.

Installation
============

If you're using this to add into your own ROM, you'll need to be sure of certain things:

- You have forked this repo and did the path changes in the opendelta.sh
- You have forked packages_apps_OpenDelta and did the server and other changes in the res/values/config.xml
- Have the delta modified by you in your manifest with the opendelta.sh copy in the root folder
- Have the packages_apps_OpenDelta in your manifest as well

Before you can use the software the first time, run in delta folder:

	chmod a+x install.sh
	chmod a+x install/xdelta3-3.0.7/configure
	./install.sh

After these you won't be tracking Github once new files were added and the script was deleted.
Don't worry about that it's OK, it will only show a report after every repo sync in the end.

Manually Usage (After building)
============

Note --> You first need a previous build to put in delta/last/$device.

Copy the build (.zip) compiled before the current one into the path: delta/last/device. 
The current one must be: out/target/product/$device, you don't need to copy anything, the output folder is already on this path.
Then, run in root folder:

	./opendelta.sh $device

When the script finished it's time to upload the contents (.delta, .sign and .update) of the 'publish' folder to the URL specified in the OpenDelta app.

Automatically (Together building)
=====

In case you're using scripts based on my ones adapted from SlimSaber.

Note --> You first need a previous build to put in delta/last/$device.

Copy the last build to the path: delta/last/$device.
Remove the out/target/product/$device in order to have a clean build.
Then, start building:

	./rom.sh $device $threads

It will ask you if you want want OpenDelta for it automatically.
Type:

	1

Or: 

	yes

In the end of the compilation it will run the same script as in manually guide but automatically.
When the script finished it's time to upload the contents (.delta, .sign and .update) of the 'publish' folder to the URL specified in the OpenDelta app.

Credits
=======

This software, as also the app OpenDelta, has been created by Chainfire. I just modified it to make it easy to use for everyone.
Original author: @Myself5 for CarbonROM.
